#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const cheerio = require('cheerio');

const regex = /stopka/i;

const rootPath = 'C:/stopka';
const capital = fs.readFileSync(path.join(rootPath, 'kapital.txt'), { encoding: 'utf8' })
const listDirectory = (rootPath) => {
    const containingElements = fs.readdirSync(rootPath)
    containingElements.forEach(element => {
        const elementPath = path.join(rootPath, element)
        if (fs.lstatSync(elementPath).isDirectory()) {
            listDirectory(elementPath)
        } else if (fs.lstatSync(elementPath).isFile() && regex.test(element)) {
            const html = fs.readFileSync(elementPath, {
                encoding: 'utf8'
            })
            const $ = cheerio.load(html);
            $('span#capital').text(capital);
            fs.writeFileSync(elementPath, $.html())
        }
    })
}

listDirectory(rootPath)